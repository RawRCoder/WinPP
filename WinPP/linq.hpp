#pragma once
#include "iterator.hpp"



template <class T> 
Iterator<T>* Where(IEnumerable<T>* collection, Func(bool(T)) condition)
{
	return reinterpret_cast<Iterator<T>*>(new WhereIterator<T>(collection->GetIterator(), condition));
}

template <class TSource, class TResult>
Iterator<TResult>* Select(IEnumerable<TSource>* collection, Func(TResult(TSource)) selector)
{
	return reinterpret_cast<Iterator<TResult>*>(new SelectIterator<TSource, TResult>(collection->GetIterator(), selector));
}

template <class T>
bool ForEachI(Iterator<T>* it, Func(bool(T)) actor)
{
	if (it->Finished())
		return true;
	do
	{
		if (!actor(it->operator()()))
			return false;

	} while (it->MoveNext());
	return true;
}
template <class T>
bool ForEach(IEnumerable<T>* c, Func(bool(T)) actor)
{
	auto it = c->GetIterator();
	auto result = ForEachI(it, actor);
	delete it;
	return result;
}


template <class T>
bool All(IEnumerable<T>* c, Func(bool(T)) condition)
{
	bool ok = true;
	ForEach(c, [&ok](T v) {
		if (condition(v))
			return true;	
		ok = false;
		return false;	
	});
	return ok;
}
template <class T>
bool Any(IEnumerable<T>* c, Func(bool(T)) condition)
{
	bool ok = false;
	ForEach(c, [&ok](T v) {
		if (!condition(v))
			return true;
		ok = true;
		return false;
	});
	return ok;
}
template <typename T>
struct RangeIterator : public Iterator<T&>
{
protected:
	T m_Start;
	T m_Value;
	i64 m_iCount;
	i64 m_iCountLeft;
	T m_Step;
public:
	RangeIterator(T begin, u64 count, T step = 1) :m_Start(begin), m_Value(begin), m_iCount(count & 0x7FFFFFFFFFFFFFFF), m_iCountLeft(m_iCount), m_Step(step) { }

	virtual bool Finished() const override { return m_iCountLeft <= 0; }
	virtual void Next() override { m_Value += m_Step; --m_iCountLeft; }
	virtual T& Current() override { return m_Value; }
	virtual void Reset() override { m_Value = m_Start; m_iCountLeft = m_Start; m_Value = m_Start; }
};

template <typename T>
Iterator<T>* Range(T begin, u64 count, T step = 1)
{
	return reinterpret_cast<Iterator<T>*>(new RangeIterator<T>(begin, count, step));
}