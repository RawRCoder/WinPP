#pragma once
#include "dynarray.hpp"

template <class T>
class Pool
{
private:
	struct Node
	{
		T m_Value;
		bool m_bInUse = false;
	};
	Node* Get(u32 id) const 
	{
		if (id >= m_Buffer.Count())
			return nullptr;
		return const_cast<Node*>(&m_Buffer[id]);
	}
	Node* GetIfUsed(u32 id) const
	{
		auto x = Get(id);
		return x->m_bInUse ? const_cast<Node*>(x) : nullptr;
	}
public:
	Pool(u32 sz = 32, bool zeroMemory = false) : m_Buffer(sz), m_iFreeSlots(sz), m_bShouldZeroMemory(zeroMemory)
	{
		if (zeroMemory)
			memset(m_Buffer.GetData(), 0, sz * sizeof Node);
		m_Buffer.Fill();
	}
	bool IsFree(u32 id) const
	{
		auto x = GetIfUsed(id);
		return !x;
	}
	bool Get(u32 id, T& outValue) const
	{
		auto x = GetIfUsed(id);
		if (!x) return false;
		outValue = x->m_Value;
		return true;
	}
	void Set(u32 id, const T& value)
	{
		if (id >= m_Buffer.Count())
			return;
		m_Buffer[id].m_Value = value;
		if (!m_Buffer[id].m_bInUse)
			--m_iFreeSlots;
		m_Buffer[id].m_bInUse = true;
	}
	void Unset(u32 id)
	{
		if (id >= m_Buffer.Count())
			return;
		if (!m_Buffer[id].m_bInUse)
			return;
		++m_iFreeSlots;
		m_Buffer[id].m_bInUse = false;		
	}
	bool Pop(u32 id, T& outValue)
	{
		auto x = GetIfUsed(id);
		if (!x) return false;
		outValue = x->m_Value;
		++m_iFreeSlots;
		x->m_bInUse = false;
		return true;
	}
	u32 AllocateIndex()
	{
		auto id = 0xFFFFFFFF;
		if (!m_iFreeSlots)
		{
			id = m_Buffer.Count();
			m_Buffer.Resize(m_Buffer.Capacity()*2, false, true);
			m_Buffer.Fill();
			m_Buffer[id].m_bInUse = true;
			m_iFreeSlots = m_Buffer.Count() - id-1;
			m_Buffer.Fill();
		}
		else {
			bool found = false;
			Repeat:
			for (; m_iNextFreeIndex < m_Buffer.Count(); ++m_iNextFreeIndex)
			{
				if (m_Buffer[m_iNextFreeIndex].m_bInUse)
					continue;
				--m_iFreeSlots;
				id = m_iNextFreeIndex;
				m_Buffer[id].m_bInUse = true;
				++m_iNextFreeIndex;
				found = true;
				break;
			}
			if (!found)
			{
				m_iNextFreeIndex = 0;
				goto Repeat;
			}
		}
		return id;
	}
	void Add(const T& value)
	{
		auto id = AllocateIndex();
		Set(id, value);
	}
	u32 Capacity() const { return m_Buffer.Count(); }
	u32 UsedSlotsCount() const { return m_Buffer.Count() - m_iFreeSlots; }
	u32 FreeSlotsCount() const { return m_iFreeSlots; }

	Pool<T>& operator+=(const T& value) { Add(value); return this; }
	T& operator[](u32 id) { auto x = GetIfUsed(id); if (!x) throw std::exception("Acces to empty slot"); return x->m_Value; }
private:
	DynArray<Node> m_Buffer;
	u32 m_iFreeSlots;	
	u32 m_iNextFreeIndex = 0;
	bool m_bShouldZeroMemory;
};