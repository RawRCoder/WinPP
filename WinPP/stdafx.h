// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once

#define TEMPLATE_DELEGATE(TS, T, NAME, ARGS) template ##TS using NAME = T(*)ARGS
#define DELEGATE(T, NAME, ARGS)			typedef T (*NAME)##ARGS
#define PREDICATE(NAME, ARGS)			DELEGATE(bool, NAME, ARGS)
#define PROCEDURE(NAME, ARGS)			DELEGATE(void, NAME, ARGS)

typedef unsigned char byte;
typedef signed char sbyte;
#if defined(_MSC_VER)
typedef unsigned __int8		u8;
typedef signed __int8		i8;
typedef unsigned __int16	u16;
typedef signed __int16  	i16;
typedef unsigned __int32	u32;
typedef signed __int32		i32;
typedef unsigned __int64	u64;
typedef signed __int64		i64;
#else
typedef unsigned char			u8;
typedef signed char				i8;
typedef unsigned short int		u16;
typedef signed short int		i16;
typedef unsigned long int		u32;
typedef signed long int			i32;
typedef unsigned long long int	u64;
typedef signed long long int	i64;
#endif
typedef float f32;
typedef double f64;


// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
