#pragma once
#include "stdafx.h"
#include <algorithm>
#include <stdexcept>
#include <functional>
#include "iterator.hpp"
#include <cstring>

template <class T>
// ������������ ����� ������������� ������������� ������ � ������������ ������� � �������� � ������������ ������ (k = 1.5)
class DynArray : public IEnumerable<T>
{
private:
	T* _pData;
	u32 _iCapacity;
	u32 _iLength;
	bool _bOwn;
public:

	// ������� ������ � ��������� ��������� ��������
	explicit DynArray(u32 cap = 10)
	{
		_pData = new T[cap];
		_iCapacity = cap;
		_iLength = 0;
		_bOwn = true;
	}

	DynArray(const DynArray<T>& src)
	{
		_pData = src._pData;
		_iCapacity = src._iCapacity;
		_iLength = src._iLength;
		_bOwn = false;
	}

	// ��������� ���������� � ������� �� ��������� ��� �� �������. ��������, ���� own=true - ������ ����� ������� ������ � ���� ��������!
	DynArray(T* objs, u32 count, bool own = false)
	{
		_pData = objs;
		_iCapacity = count;
		_iLength = count;
		_bOwn = own;
	}

	// �� �������� ���������� ��������� ������!
	~DynArray()
	{
		if (_bOwn)
		{
			delete[] _pData;
			_bOwn = false;
			_pData = nullptr;
		}
	}

	// ���������� ��������� � ������ ( === Length())
	u32 Count() const { return _iLength; }

	// ���������� ��������� � ������ ( === Count())
	u32 Length() const { return _iLength; }

	// ������� ������������ ����������� ������
	u32 Capacity() const { return _iCapacity; }

	void Fill() { _iLength = _iCapacity; }

	void Fill(const T& value)
	{
		while (_iLength!=_iCapacity)
		{
			Add(value);
		}
	}

	void Fill(std::function<const T&(u32)> f)
	{
		while (_iLength != _iCapacity)
		{
			Add(f(_iLength));
		}
	}

	// ���� ������ ��������, ���������� ������������� ������. k = 1.5.
	// �������������, ��� ����� ������ ����� ������ ������ �������.
	void Grow()
	{
		if (_iCapacity <= _iLength || !_bOwn)
		{
			auto maxv = std::max<u32>(_iCapacity + _iCapacity / 2, _iCapacity + 1);
			Resize(maxv);
		}
	}

	void EnsureCapacity(u32 newCapacity)
	{
		if (newCapacity >= Capacity())
			Resize(newCapacity);
	}

	// ��������� ������� � ����� ������.
	void Add(const T& element)
	{
		Grow();
		_pData[_iLength] = element;
		++_iLength;
	}

	// ��������� �������� � ����� ������.
	void AddRange(const T* elements, u32 count)
	{
		auto targetLength = _iLength + count;
		if (targetLength > _iCapacity)
			Resize(std::max<u32>(_iCapacity + _iCapacity / 2, targetLength));
		u32 i = 0;
		while (i < count)
		{
			_pData[_iLength + i] = elements[i];
			++i;
		}
		_iLength += i;
	}

	// ��������� �������� � ����� ������.
	void AddRange(const DynArray<T>* elements)
	{
		u32 n = elements->_iLength;
		auto targetLength = _iLength + n;
		if (targetLength <= _iCapacity)
			Resize(std::max<u32>(_iCapacity + _iCapacity / 2, targetLength));
		u32 i = 0;
		while (i < n)
		{
			_pData[_iLength + i] = elements->_pData[i];
			++i;
		}
	}

	// ��������� ������� � ������������ ����� ������. ��� index >= ����� ������ - ������� �������� Add
	void Insert(const T& element, const u32 index)
	{
		if (index >= _iLength)
		{
			Add(element);
			return;
		}

		Grow();

		for (auto i = static_cast<i32>(_iLength) - 1; i >= static_cast<i32>(index); --i)
			_pData[i + 1] = _pData[i];

		_pData[index] = element;
		_iLength++;
	}

	// ���������� ������ ������� �������� � ������, ������� �������, ���� -1, ���� ������ �������� � ������ ���
	i32 IndexOf(const T& element) const
	{
		for (u32 i = 0; i < _iLength; ++i)
		{
			if (_pData[i] == element)
				return i;
		}
		return -1;
	}

	// ������� ������� � ��������� ��������. �� �������� ����������. ���������� ��������� �� ������ �������
	T PopAt(const u32 index)
	{
		if (index >= _iLength)
			throw std::out_of_range("Index is out of range");

		T pObject = _pData[index];
		--_iLength;

		for (u32 i = index; i < _iLength; ++i)
			_pData[i] = _pData[i + 1];

		return pObject;
	}

	// ������� ������� � ��������� ��������. �� �������� ����������.
	void RemoveAt(const u32 index)
	{
		if (index >= _iLength)
			throw std::out_of_range("Index is out of range");

		--_iLength;

		for (u32 i = index; i < _iLength; ++i)
			_pData[i] = _pData[i + 1];
	}

	// ������� � ��������������� ������� � ��������� ��������.
	void DeleteAt(const u32 index)
	{
		T pObject = PopAt(index);
		if (pObject != nullptr)
			delete pObject;
	}

	// ������� �� ������ ������ �������, ������ �������. ������������ ��������� � ��������� �������.
	// ���� ����� ��������� ���, ����������� OutOfRange exception
	T Remove(const T& element)
	{
		return PopAt(IndexOf(element));
	}

	// ������� �� ������ ������ �������, ������ �������. ���� ����� ��������� ���, ������������ false, ����� true. 
	// � removed ������������ ��������� �������
	bool Remove(const T& element, T& removed)
	{
		i32 id = IndexOf(element);
		if (id < 0) return false;

		removed = PopAt((u32)id);
		return true;
	}

	// ������� � ��������������� �� ������ ������ �������, ������ �������. ���� ����� ��������� ���, ����������� OutOfRange exception
	void Delete(const T& element)
	{
		DeleteAt(IndexOf(element));
	}

	// ������� � ��������������� �� ������ ������ �������, ������ �������. ���� ����� ��������� ���, ������������ false, ����� true. 
	bool rDelete(const T& element)
	{
		i32 id = IndexOf(element);
		if (id < 0) return false;
		DeleteAt(static_cast<u32>(id));
		return true;
	}

	// ������� ������ (�� ����������� ������ ���������)
	void Clear()
	{
		for (u32 i = _iLength; i > 0; --i)
			RemoveAt(i - 1);
	}

	// ������� ������ (����������� ������ ���������)
	void ClearDestroy()
	{
		for (u32 i = _iLength; i > 0; --i)
			DeleteAt(i - 1);
	}

	// ������������ ������ ��� ������, ������� ��� ����� ������������ ������
	void Resize(const u32 nsz, bool nocopy = false, bool zeroMemory = false)
	{
		T* _pNewData = new T[nsz];
		if (zeroMemory)
		{
			memset(_pNewData, 0, nsz*sizeof T);
		}
		if (!nocopy)
		{
			u32 end = std::min<u32>(nsz, _iCapacity);
			for (u32 i = 0; i < end; ++i)
				_pNewData[i] = _pData[i];
		}
		else _iLength = 0;

		if (_bOwn)
		{
			delete[] _pData;
			_pData = nullptr;
			_bOwn = false;
		}
		_pData = _pNewData;
		_bOwn = true;
		_iCapacity = nsz;
		if (_iLength > _iCapacity)
			_iLength = _iCapacity;
	}

	// ���������� ������ �� ������� � �������� ��������.
	T& operator[](u32 id)
	{
		if (id >= _iLength)
			throw std::out_of_range("Index is out of range");
		return _pData[id];
	}
	// ���������� ������ �� ������� � �������� ��������.
	const T& operator[](u32 id) const
	{
		if (id >= _iLength)
			throw std::out_of_range("Index is out of range");
		return _pData[id];
	}

	// ���������� ���� ������ � ���� ������� � ������ == Capacity() (��� �����������).
	T* GetData() const { return _pData; }

	// ���������� ����� ������ � ���� ������� � ������ == Length().
	const T* GetDataCopy()
	{
		auto arr = new T[_iLength];
		for (u32 i = 0; i < _iLength; ++i)
			arr[i] = _pData[i];
		return arr;
	}


	DynArray<T>& operator+=(const T& value)
	{
		Add(value);
		return *this;
	}
	DynArray<T>& operator-=(const T& value)
	{
		Remove(value);
		return *this;
	}

	i32 AddRange(IEnumerable<T>& src, u32 skip = 0, u32 take = 0xFFFFFFFF)
	{
		auto count = 0;
		src.ForEach([this, &skip, &take, &count](T t)
		{
			if (skip > 0)
			{
				--skip;
				return true;
			}
			this->Add(t); 
			--take; 
			++count;
			return take > 0;
		});
		return count;
	}

	struct ArrayIterator : public Iterator<T&>
	{
	protected:
		typedef DynArray<T> _Collection;
	public:
		ArrayIterator(_Collection* array) : m_pArray(array) { }

		virtual bool Finished() const override { return id >= m_pArray->Count(); }
		virtual void Next() override { ++id; }
		virtual T& Current() override { return m_pArray->operator[](id); }
		virtual void Reset() override { id = 0; }
	protected:
		_Collection* m_pArray;
		u32 id = 0;
	};

	virtual Iterator<T>* GetIterator() override {
		return reinterpret_cast<Iterator<T>*>(new ArrayIterator(this));
	}
};