#pragma once
#include "dynarray.hpp"
#include "linq.hpp"

template <class TArgs>
using DSimpleAction = std::function<void(TArgs&)>;
template <class TObject, class TArgs>
using DObjectAction = std::function<void(TObject*, TArgs&)>;

template <class TArgs>
bool operator==(const DSimpleAction<TArgs>& a, const DSimpleAction<TArgs>& b)
{
	return a.target<void(TArgs&)>() == b.target<void(TArgs&)>();
}
template <class TObject, class TArgs>
bool operator==(const DObjectAction<TObject, TArgs>& a, const DObjectAction<TObject, TArgs>& b)
{
	return a.target<void(TObject*, TArgs&)>() == b.target<void(TObject*, TArgs&)>();
}

template <class TArgs>
class Event
{
public:
	typedef DSimpleAction<TArgs> _FuncType;
	Event() {}
	virtual ~Event() {}

	void operator() (TArgs& args)
	{
		m_Handlers.ForEach([&args](_FuncType func) { func(args); return true; });
	}

	Event& operator+=(_FuncType handler)
	{
		m_Handlers += handler;
		return *this;
	}
	Event& operator-=(_FuncType handler)
	{
		m_Handlers -= handler;
		return *this;
	}
private:
	DynArray<_FuncType> m_Handlers;
};


template <class TObject, class TArgs>
class ObjectEvent
{
public:
	typedef DObjectAction<TObject, TArgs> _FuncType;

	ObjectEvent() {}
	virtual ~ObjectEvent() {}

	void operator() (TObject* object, TArgs& args)
	{
		m_Handlers.ForEach([&args, &object](_FuncType func) { func(object, args); return true; });
	}

	ObjectEvent& operator+=(_FuncType handler)
	{
		m_Handlers += handler;
		return *this;
	}
	ObjectEvent& operator-=(_FuncType handler)
	{
		m_Handlers -= handler;
		return *this;
	}
private:
	DynArray<_FuncType> m_Handlers;
};