#include "stdafx.h"
#include "path.hpp"
#include <windows.h>
#include <Shlwapi.h>
#include <algorithm>

#pragma comment (lib, "Shlwapi.lib")

std::wstring	Path::m_sPath;
std::wstring	Path::m_sExePath;
Path*			Path::m_pSingleton = new Path();

std::wstring Path::ExecutablePath() { return m_sExePath; }
std::wstring Path::ExecutableDirectoryPath() { return m_sPath; }

std::wstring Path::Combine(const std::wstring& root, const std::wstring& subpath)
{
	wchar_t output[MAX_PATH];
	if (!PathCombineW(output, root.c_str(), subpath.c_str()))
		throw std::exception("Failed to combine paths");
	return std::wstring(output);
}

std::string Path::Combine(const std::string& root, const std::string& subpath)
{
	char output[MAX_PATH];
	if (!PathCombineA(output, root.c_str(), subpath.c_str()))
		throw std::exception("Failed to combine paths");
	return std::string(output);
}

std::wstring Path::LocalPath(const std::wstring& subpath) { return Combine(m_sPath, subpath); }

void Path::SplitPath(const std::wstring& path, std::wstring& outDirectory, std::wstring& outFileName, std::wstring& outExt)
{
	outFileName = GetFileNameWithoutxtension(path);
	outDirectory = GetDirectoryName(path);
	outExt = GetExtension(path);
}
void Path::SplitPath(const std::string& path, std::string& outDirectory, std::string& outFileName, std::string& outExt)
{
	outFileName = GetFileNameWithoutExtension(path);
	outDirectory = GetDirectoryName(path);
	outExt = GetExtension(path);
}

std::wstring Path::GetExtension(const std::wstring& path)
{
	auto ptr = PathFindExtensionW(path.c_str());
	auto ext = ptr ? std::wstring(ptr + 1) : std::wstring(L"");
	return ext;
}

std::string Path::GetExtension(const std::string& path)
{
	auto ptr = PathFindExtensionA(path.c_str());
	auto ext = ptr ? std::string(ptr + 1) : std::string("");
	return ext;
}

std::wstring Path::GetDirectoryName(const std::wstring& path)
{
	auto dir = new wchar_t[path.length() + 1];
	wcscpy_s(dir, path.length() + 1, path.c_str());
	PathRemoveFileSpecW(dir);
	return dir;
}

std::string Path::GetDirectoryName(const std::string& path)
{
	auto dir = new char[path.length() + 1];
	strcpy_s(dir, path.length() + 1, path.c_str());
	PathRemoveFileSpecA(dir);
	return dir;
}

std::wstring Path::GetFileName(const std::wstring& path)
{
	auto fileName = new wchar_t[path.length() + 1];
	wcscpy_s(fileName, path.length() + 1, path.c_str());
	PathStripPathW(fileName);
	return fileName;
}

std::string Path::GetFileName(const std::string& path)
{
	auto fileName = new char[path.length() + 1];
	strcpy_s(fileName, path.length() + 1, path.c_str());
	PathStripPathA(fileName);
	return fileName;
}

std::wstring Path::GetFileNameWithoutxtension(const std::wstring& path)
{
	auto fileName = new wchar_t[path.length() + 1];
	wcscpy_s(fileName, path.length() + 1, path.c_str());
	PathStripPathW(fileName);
	PathRemoveExtensionW(fileName);
	return fileName;
}

std::string Path::GetFileNameWithoutExtension(const std::string& path)
{
	auto fileName = new char[path.length() + 1];
	strcpy_s(fileName, path.length() + 1, path.c_str());
	PathStripPathA(fileName);
	PathRemoveExtensionA(fileName);
	return fileName;
}

bool Path::HasExtension(const std::wstring& path)
{
	return PathFindExtensionW(path.c_str()) != nullptr;
}

Path::Path()
{
	wchar_t path[512];
	ZeroMemory(path, sizeof path);
	u32 pathLen = GetModuleFileNameW(nullptr, path, sizeof path);
	m_sExePath = std::wstring(path);
	m_sPath = GetDirectoryName(path);
}
