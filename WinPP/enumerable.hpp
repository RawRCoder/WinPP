#pragma once
#include <memory>

#define Func(T) std::function<T>

template <class T>
	struct Iterator;

template <class T>
	struct IEnumerable;

template <class T> 
	Iterator<T>* Where(IEnumerable<T>* collection, Func(bool(T)) condition);

template <class TSource, class TResult>
	Iterator<TResult>* Select(IEnumerable<TSource>* collection, Func(TResult(TSource)) selector);

template <class T>
	bool ForEach(IEnumerable<T>* c, Func(bool(T)) actor);

template <class T>
	bool All(IEnumerable<T>* c, Func(bool(T)) condition);

template <class T>
	bool Any(IEnumerable<T>* c, Func(bool(T)) condition);



template <class T>
struct IEnumerable
{
public:
	virtual ~IEnumerable() { }
	virtual Iterator<T>* GetIterator() = 0;

	Iterator<T>* Where(Func(bool(T)) condition) { return ::Where(this, condition); }
	template <class TResult> Iterator<TResult>* Select(Func(TResult(T)) selector) { return ::Select(this, selector); }
	bool ForEach(Func(bool(T)) actor) { return ::ForEach(this, actor); }
	bool All(IEnumerable<T>* c, Func(bool(T)) condition) { return ::All(this, condition); }
	bool Any(Func(bool(T)) condition) { return ::Any(this, condition); }
};