#pragma once
#include "stdafx.h"
#include <functional>
#include "enumerable.hpp"


template <class T>
struct Iterator : public IEnumerable<T>
{
public:
	virtual ~Iterator() { }
	virtual bool Finished() const = 0;
	virtual void Next() = 0;
	virtual T& Current() = 0;
	virtual void Reset() = 0;
	virtual Iterator<T>* GetIterator() override {
		return this;
	}

	bool MoveNext() { Next(); return !Finished(); }

	T& operator()() { return Current(); }
	Iterator<T>& operator++()
	{
		Next();
		return this;
	}
};

template <class TIn, class TOut>
struct ComplexIterator : Iterator<TOut>
{
private:
	Iterator<TIn>* m_BaseIterator;
protected:
	Iterator<TIn>* GetBase() const { return m_BaseIterator; }
	ComplexIterator(Iterator<TIn>* baseIterator) : m_BaseIterator(baseIterator)
	{}
	TIn& BasicCurrent() { return m_BaseIterator->Current(); }
public:
	virtual ~ComplexIterator() override { if (m_BaseIterator) delete m_BaseIterator; }
	virtual bool Finished() const override { return m_BaseIterator->Finished(); }
	virtual void Next() override { m_BaseIterator->Next(); }
	virtual void Reset() override { m_BaseIterator->Reset(); }
};

template <class T>
struct WhereIterator : ComplexIterator<T, T>
{
private:
	Func(bool(T)) m_Condition;

	void SkipUntilValid()
	{
		while (!__super::Finished())
		{
			__super::Next();
			if (__super::Finished())
				break;
			if (m_Condition(__super::BasicCurrent()))
				break;
		}
	}
public:
	WhereIterator(Iterator<T>* baseIterator, Func(bool(T)) condition) : ComplexIterator(baseIterator)
	{
		m_Condition = condition;
		if (!m_Condition(__super::BasicCurrent()))
			SkipUntilValid();
	}
	virtual T& Current() override { return __super::BasicCurrent(); }
	virtual void Next() override { SkipUntilValid(); }	
};

template <class TSource, class TResult>
struct SelectIterator : ComplexIterator<TSource, const TResult>
{
private:
	Func(TResult(TSource)) m_Selector;
	TResult m_Current;
public:
	SelectIterator(Iterator<TSource>* baseIterator, Func(TResult(TSource)) selector) : ComplexIterator(baseIterator)
	{
		m_Selector = selector;
	}
	virtual const TResult& Current() override { m_Current = m_Selector(__super::BasicCurrent()); return m_Current; }
};


#define FOR_EACH(It, Collection) for (auto It = (Collection).GetIterator(); !It.Finished(); ++It)
#define FOR_EACH_P(It, Collection) for (auto It = (Collection)->GetIterator(); !It.Finished(); ++It)