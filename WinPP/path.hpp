#pragma once
#include <string>

class Path
{
public:
	static std::wstring ExecutablePath();
	static std::wstring ExecutableDirectoryPath();
	static std::wstring Combine(const std::wstring& root, const std::wstring& subpath);
	static std::string Combine(const std::string& root, const std::string& subpath);
	static std::wstring LocalPath(const std::wstring& subpath);
	static void SplitPath(const std::wstring& path, std::wstring& outDirectory, std::wstring& outFileName, std::wstring& outExt);
	static void SplitPath(const std::string& path, std::string& outDirectory, std::string& outFileName, std::string& outExt);

	static std::wstring GetExtension(const std::wstring& path);
	static std::string GetExtension(const std::string& path);

	static std::wstring GetDirectoryName(const std::wstring& path);
	static std::string GetDirectoryName(const std::string& path);

	static std::wstring GetFileName(const std::wstring& path);
	static std::string GetFileName(const std::string& path);

	static std::wstring GetFileNameWithoutxtension(const std::wstring& path);
	static std::string GetFileNameWithoutExtension(const std::string& path);

	static bool HasExtension(const std::wstring& path);
private:
	Path();
	static Path* m_pSingleton;
	static std::wstring m_sPath;
	static std::wstring m_sExePath;
};
static std::wstring operator/(const std::wstring& root, const std::wstring& subpath)
{
	return Path::Combine(root, subpath);
}
static std::string operator/(const std::string& root, const std::string& subpath)
{
	return Path::Combine(root, subpath);
}
