
#include "pool.hpp"
#include <string>
#include <chrono>
#include <linq.hpp>

template<typename TimeT = std::chrono::milliseconds>
static typename TimeT::rep measureFunc(std::function<void()> func)
{
	auto start = std::chrono::system_clock::now();
	func();
	auto duration = std::chrono::duration_cast<TimeT>(
		std::chrono::system_clock::now() - start);
	return duration.count();
}

int main()
{
	getchar();
	Range(0, 10)
		->Where([](i32 x) { return !(x % 2); })
		->Select<i32>([](i32 x) {return x*x;})
		->ForEach([](i32 x) {
		printf((std::to_string(x) + "\r\n").c_str());
		return true;});
	getchar();
}